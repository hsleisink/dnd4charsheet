Character Sheet for Dungeons & Dragons 4th edition
==================================================

These are the files I use to create a character sheet for the 4th edition of
Dungeons & Dragons. The PDF file is an original character sheet file. The ODS
files are created by me and are publised under the CC BY 4.0 license. The Power
Cards file can be used to create a power cards sheet via templates. The Stats
details can be used to keep track of all the stats in the character sheet and
how they are calculated.
